

(function(root){
   
    
    function setEvent(ev,v){
        var nodeData = ev.split(' ');
        var nodeValue = parseInt(v.value) || v;
        return function(e){
            for(var i=0,j=nodeData.length;i<j;i++){
                game.actions[nodeData[i]](e.target,nodeValue,this);
            }
        };
    }
    
    function getData(node){
        var attrs = node.attributes || [],
            data = {};
        
        if(!attrs.length) return data;
        each(attrs, function(i, attr){
            var name = attr.nodeName;
            if(/^data-event/.test(name)){
                data.events = data.events || {};
                data.events[name.replace(/^data-/,'')] = attr.value;
            }else if(/^data-action/.test(name)){
                data.actions = data.actions || {};
                data.actions[name.replace(/^data-/,'')] = attr.value;
            }else if(/^data-value/.test(name)){
                data.values = data.values || {};
                data.values[name.replace(/^data-/,'')] = attr.value;
            }
            
        });
        return data;
    }
    
    var wrapper = document.querySelectorAll('[data-action]');
    for(var i =0,j=wrapper.length;i<j;i++){
        
        var node = wrapper[i];
        var data = getData(node);
        var value = data.values || 0;
        var action = setEvent(data.actions.action, value );
        node.addEventListener(data.events.event, action);
    }
    
    document.addEventListener('DOMNodeInserted',function(e){
           if(e.target.hasAttributes()){
              
               var node = e.target;
               if(!/^data-/.test(node.attributes[0].nodeName)) return;
               var data = getData(node);
               var value = data.values || 0;
               var action = setEvent(data.actions.action, value );
               node.addEventListener(data.events.event, action);
           }
    });
    
}());

