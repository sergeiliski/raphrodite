
window.game = window.game || {};
(function(game){
    
    document['dwedf23wrt34t'] = Math.random() * 1000 | 0;
    
    
    function getWrapper(nodename){
        var node = document.getElementById(nodename);
        if(node) return node;
        return false;
    }
    
    function createSandbox(){
        var node = getWrapper('wrapper');
        var sandbox = $gin.html('<div>').attr('class','sandbox').prepend(node);
        var narrator = $gin.html('<div>').attr('class','narrator').prepend(node);
        
        game.sandbox = sandbox;
        game.narrator = narrator;
    }
    
    
    createSandbox();
    
    game.actions = {};
    
    
    
    
}(window.game));