
(function(root){

    
    package('items');
    
    intern('Pistol', function(){
        /*
        *   PISTOL ITEM
        */
        var name = 'A Pistol';
        
        var ammo = 0;

        function shoot(target){
            if(ammo <= 0) return game.sandbox.text('The gun has no ammo.');
            if(!target) return game.sandbox.text('You have no target to shoot at.');
            game.sandbox.text('You shot a gun!');
            
        }
        
        function reload(){
            if(ammo <= 0) return game.sandbox.text('You have no bullets to reload with.');
        }
        
        function examine(){
            game.sandbox.text('A semi-automatic blowback-operated firearm. You currenly have '+ammo+ ' bullets for this gun.');
        }
        
        function noAmmo(){
            
        }
        
        return {
            shoot:shoot,
            reload:reload,
            noAmmo:noAmmo,
            examine:examine,
            name:name
            
        };
        
    });
    
    
    
    intern('Medpack', function(){
        /*
        *   MEDPACK ITEM
        */
        var name = 'A Medpack';
        
        
        function use(node){
                node.textContent = 'empty';
                node.parentNode.removeChild(node.parentNode.lastChild);
                delete node;
                game.sandbox.text('You used a Medkit.');
            
        }
        
        function examine(){
            game.sandbox.text('A medpack is a first aid kit that contains limited-utility diagnosis equipment and medicine.');
        }
        
        
        return {
            use:use,
            examine:examine,
            name: name
        };

    });
    
    
    intern('Cryptic Message', function(){
        /*
        *   CRYPTIC MESSAGE ITEM
        */
        var name = 'A Cryptic Message';
        
        function use(){
            game.sandbox.text('Cannot use this with anything.');
        }

        function examine(){
            game.sandbox.text('A thin paper-like material made from the pith of the papyrus plant. It contains sporadic writings.');
        }


        return {
            use:use,
            examine:examine,
            name:name
        };

    });
    
    
    intern('Bottle', function(){
        /*
        *   Bottle
        */
        var name = 'A Bottle';

        function use(){

        }

        function examine(){

        }


        return {
            use:use,
            examine:examine,
            name:name
        };

    });



   
}());
